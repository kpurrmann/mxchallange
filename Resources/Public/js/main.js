$(function() {


    if ($('#form-success').length > 0) {
        $('html, body').animate({
            scrollTop: $('#form-success').offset().top
        });
    }

    if ($('form .alert-danger').length > 0) {
        $('html, body').animate({
            scrollTop: $('form .alert-danger:first-child').parent().offset().top
        });
    }
    ;

    if ($('form .panel-group').length > 0) {
        $('form .panel-group').each(function(index, element) {
            if ($(element).find('.indicator').val() === '1') {
                $(element).find('.collapse').collapse('show');
            }
        });
    }

    $('#begleitung, #rechnung').on('hidden.bs.collapse', function() {
        $(this).find('.indicator').val('0');
    });

    $('#begleitung, #rechnung').on('shown.bs.collapse', function() {
        $(this).find('.indicator').val('1');
    });



//    $.fn.setCarouselCaption = function(el, text) {
//        $('#figcaption').text($('.carousel:not(.hidden) .carousel-inner>.item.active').find('figcaption').text());
//    };

    if ($(window).width() >= 1100) {
        $('#button-navigation>li:not(.active), #main-navigation>li:not(.active)').mouseover(function() {
            if ($(this).find('.sub-navigation').length > 0) {
                $('#button-navigation>li.active .sub-navigation, #main-navigation>li.active .sub-navigation ').hide();
            }
        });
        $('#button-navigation>li:not(.active), #main-navigation>li:not(.active)').mouseout(function() {
            $('#button-navigation li.active .sub-navigation, #main-navigation>li.active .sub-navigation').show();
        });
    }


    $('.gallery-navigation').on('click', 'a', function(e) {
        $('.carousel-gallery:visible').toggleClass('hidden');
        var gallery = $(this).attr('href');
        $(gallery).toggleClass('hidden');
        $(this).toggleClass('active').siblings('.active').toggleClass('active');
//        $(this).setCarouselCaption();
        e.preventDefault();
    });

    $('.btn-group label input[selected=selected]').parent('label').addClass('active');

//
//    $(this).setCarouselCaption();
//    $('.carousel:visible').on('slid.bs.carousel', function() {
//        $(this).setCarouselCaption();
//    });


    $(".form-group .dropdown-menu").on('click', 'li a', function(e) {
        $(this).parents('.btn-group').children('.btn:first-child').children('span:first-child').text($(this).text());
        $(this).parents('.btn-group').children('input').val($(this).text());
        e.preventDefault();
    });
    
    $('input, textarea').placeholder();



});
