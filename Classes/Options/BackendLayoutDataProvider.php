<?php

namespace KPU\Mxchallenge\Options;

/*
 * Copyright (C) 2014 Kevin Purrmann <entwicklung@purrmann-websolutions.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use \TYPO3\CMS\Backend\View\BackendLayout\BackendLayout;
use \TYPO3\CMS\Backend\View\BackendLayout\DataProviderContext;
use \TYPO3\CMS\Backend\View\BackendLayout\BackendLayoutCollection;
use \TYPO3\CMS\Backend\Utility\BackendUtility;

/**
 * Description of BackendLayoutDataProvider
 *
 * @author Kevin Purrmann <entwicklung@purrmann-websolutions.de>
 */
class BackendLayoutDataProvider implements \TYPO3\CMS\Backend\View\BackendLayout\DataProviderInterface
{

    protected $backendLayouts = array(
        'mx-start' => array(
            'title' => 'MX Challenge Website Layout',
            'config' => '
                backend_layout {
                colCount = 6
                rowCount = 4
                rows {
                    1 {
                        columns {
                            1 {
                                name = Header/Teaser (wird vererbt)
                                colspan = 6
                                colPos = 100
                            }
                        }
                    }
                    2 {
                        columns {
                            1 {
                                name = Inhaltsbereich (Zur Gliederung Gridelements verwenden)
                                colPos = 110
                                colspan = 6
                            }
                        }
                    }
                    3 {
                        columns {
                            1 {
                                name = Grauer Bereich Unten (nur angezeigt wenn Inhalt vorhanden)
                                colPos = 120
                                colspan = 6
                            }
                        }
                    }
                    4 {
                        columns {
                            1 {
                                name = Footer Spalte 1 (wird vererbt)
                                colPos = 210
                            }
                            2 {
                                name = Footer Spalte 2 (wird vererbt)
                                colPos = 220
                            }
                            3 {
                                name = Footer Spalte 3 (wird vererbt)
                                colPos = 230
                            }
                            4 {
                                name = Footer Spalte 4 (wird vererbt)
                                colPos = 240
                            }
                            5 {
                                name = Footer Spalte 5 (wird vererbt)
                                colPos = 250
                            }
                            6 {
                                name = Footer Spalte 6 (wird vererbt)
                                colPos = 260
                            }
                        }
                    }
                }
            }',
            'icon' => ''
        )
    );

    /**
     * @param DataProviderContext $dataProviderContext
     * @param BackendLayoutCollection $backendLayoutCollection
     * @return void
     */
    public function addBackendLayouts(DataProviderContext $dataProviderContext, BackendLayoutCollection $backendLayoutCollection)
    {
        foreach ($this->backendLayouts as $key => $data) {
            $data['uid'] = $key;
            $backendLayout = $this->createBackendLayout($data);
            $backendLayoutCollection->add($backendLayout);
        }
    }

    /**
     * Gets a backend layout by (regular) identifier.
     *
     * @param string $identifier
     * @param integer $pageId
     * @return NULL|BackendLayout
     */
    public function getBackendLayout($identifier, $pageId)
    {
        $backendLayout = NULL;
        if (array_key_exists($identifier, $this->backendLayouts)) {
            return $this->createBackendLayout($this->backendLayouts[$identifier]);
        }
        return $backendLayout;
    }

    /**
     * Creates a new backend layout using the given record data.
     *
     * @param array $data
     * @return BackendLayout
     */
    protected function createBackendLayout(array $data)
    {
        $backendLayout = BackendLayout::create($data['uid'], $data['title'], $data['config']);
        $backendLayout->setIconPath($this->getIconPath($data['icon']));
        $backendLayout->setData($data);
        return $backendLayout;
    }

    /**
     * Gets and sanitizes the icon path.
     *
     * @param string $icon Name of the icon file
     * @return string
     */
    protected function getIconPath($icon)
    {
        $iconPath = '';
        if (!empty($icon)) {
            $iconPath = $icon;
        }
        return $iconPath;
    }

}
