<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$TCA['tx_bootstrappackage_carousel_item']['columns']['background_color']['config']['default'] = 'transparent';
$TCA['tx_bootstrappackage_carousel_item']['columns']['image']['config'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'image', array(
            'foreign_types' => array(
                '0' => array(
                    'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                ),
                \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
                    'showitem' => '
                                --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                --palette--;;filePalette'
                ),
            ),
            'minitems' => 0,
            'maxitems' => 9999,
                ), $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
);

$TCA['tt_content']['types']['bootstrap_package_carousel']['showitem'] = "
    --palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
    bodytext;LLL:EXT:cms/locallang_ttc.xlf:bodytext_formlabel;;richtext:rte_transform[flag=rte_enabled|mode=ts_css], rte_enabled;LLL:EXT:cms/locallang_ttc.xlf:rte_enabled_formlabel,
    --palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.headers;bootstrap_package_header,
    tx_bootstrappackage_carousel_item,
    --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.appearance,
    --palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.frames;frames,
    --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
    --palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.visibility;visibility,
    --palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.access;access,
    --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.extended,
    --div--;LLL:EXT:lang/locallang_tca.xlf:sys_category.tabs.category,
    categories
";

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'MX Challenge Website');

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['BackendLayoutDataProvider'][$_EXTKEY] = 'KPU\Mxchallenge\Options\BackendLayoutDataProvider';
