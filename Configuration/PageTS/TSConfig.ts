TCEFORM {
    tt_content {
        menu_type {
            removeItems = 
            addItems {
                column = Mehrere Teaser mit Bild 1/3 Breite
                full_column = Einzelner Teaser mit Bild Volle Breite
            }
        }
    layout {
        types {
            bootstrap_package_carousel {
                addItems {
                    120 = Galerie Ansicht
                }
            }
        }
    }
    }
}
#############
#### RTE ####
#############
RTE {

    // Default RTE configuration (all tables)
    default {

        // Default target for links
        defaultLinkTarget = _top

        // Buttons to show
        showButtons = *

        // Toolbar order
        toolbarOrder = formatblock, blockstyle, textstyle, linebreak, bold, italic, underline, strikethrough, bar, orderedlist, unorderedlist, bar, left, center, right, copy, cut, paste, bar, undo, redo, removeformat, bar, link, unlink, linkcreator, bar, imageeditor, bar, line, insertparagraphbefore, insertparagraphafter, bar, chMode

        RTEHeightOverride = 700
        RTEWidthOverride = 700

        //hide / show HTML tag
        buttons.formatblock.orderItems = h1, h2, h3, h4, h5, h6, p, quotation
        buttons.textstyle.tags.span.allowedClasses = label
        buttons.textstyle.tags.REInlineTags >
        buttons.textstyle.REInlineTags >
        buttons.blockstyle.tags.table.allowedClasses >
        buttons.left.useClass = text-left
        buttons.center.useClass = text-center
        buttons.right.useClass = text-right

        // Disable contextual menu
        contextMenu.disabled = 1
        showStatusBar = 1
        contentCSS = typo3conf/ext/mxchallenge/Resources/Public/css/rte.css
        removeTagsAndContents =
        useCSS = 1

        // Processing rules
        proc {

            allowedClasses  < RTE.default.classesCharacter
            overruleMode = ts_css
            dontConvBRtoParagraph = 1
            remapParagraphTag = p
            allowTags = a, abbr, acronym, address, blockquote, b, br, caption, cite, code, div, em, font, h1, h2, h3, h4, h5, h6, hr, i, img, li, link, ol, p, pre, q, sdfield, span, strike, strong, sub, sup, u, ul
            denyTags >
            keepPDIVattribs = xml:lang,class,style,align
            allowTagsOutside = img,hr,h1,h2,h3,h4,h5,h6,br,ul,ol,li,pre,address,span,blockquote
            allowTagsInTypolists = br,font,b,i,u,a,img,span
            dontRemoveUnknownTags_db = 1
            preserveTables = 0

            entryHTMLparser_db = 1
            entryHTMLparser_db {
                allowTags < RTE.default.proc.allowTags
                denyTags >
                htmlSpecialChars = 0
                tags.img >
                tags.div.allowedAttribs = class,style,align
                tags.p.allowedAttribs = class,style,align
                removeTags = center, font, o:p, sdfield, strike, u
                keepNonMatchedTags = protect
            }
            HTMLparser_db {
                noAttrib = br
                xhtml_cleaning = 1
            }
            exitHTMLparser_db = 1
            exitHTMLparser_db {
                tags.b.remap = strong
                tags.i.remap = em
                keepNonMatchedTags = 1
                htmlSpecialChars = 0
            }

        }
    }
}

// Frontend RTE configuration
RTE.default.FE < RTE.default