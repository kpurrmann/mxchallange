/*
 *    Project:	mx-challenge - mx-challenge
 *    Version:	1.0.0
 *    Date:		24.07.2014 18:31:38
 *    Author:	Kevin Purrmann <entwicklung@purrmann-websolutions.de> 
 *
 *    Coded with Netbeans!
 */

page {
    shortcutIcon = EXT:mxchallenge/Resources/Public/Icons/mxfavicon.ico
    includeCSS {
        theme >
        fonts = https://fonts.googleapis.com/css?family=Oxygen:400,700,300
        fonts.external = 1
        fonts.disableCompression = 1

        bootstrap = EXT:mxchallenge/Resources/Public/css/bootstrap.min.css
        bootstraptheme = EXT:mxchallenge/Resources/Public/css/bootstrap-theme.min.css
        mxchallenge = EXT:mxchallenge/Resources/Public/css/main.css
    }
    includeJSlibs {
        modernizr >
        modernizr = EXT:mxchallenge/Resources/Public/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js
        modernizr.forceOnTop = 1    
    }
    includeJSFooter {
        placeholder = EXT:mxchallenge/Resources/Public/js/vendor/placeholder.js
        mainJS = EXT:mxchallenge/Resources/Public/js/main.js
    }

    10.file.stdWrap.cObject {
        mxchallenge__mx-start = TEXT
        mxchallenge__mx-start.value = EXT:mxchallenge/Resources/Private/Templates/Page/Standard.html
        mxchallenge__mx-start.insertData = 1

        default = TEXT
        default.value = EXT:mxchallenge/Resources/Private/Templates/Page/1-Column.html
        default.insertData = 1
    }
}

################
#### CONFIG ####
################
config {
    no_cache                            = {$config.no_cache}
    uniqueLinkVars                      = 1
    pageTitleFirst                      = 1
    linkVars                            = L
    renderCharset                       = utf-8
    metaCharset                         = utf-8
    doctype                             = html5
    removeDefaultJS                     = external
    inlineStyle2TempFile                = 1
    admPanel                            = 0
    debug                               = 0
    cache_period                        = 43200
    sendCacheHeaders                    = 0
    intTarget                           =
    extTarget                           =
    disablePrefixComment                = 1
    index_enable                        = 1
    index_externals                     = 1
    headerComment                       = {$config.headerComment}
    baseURL                             =  {$config.baseURI}


    // Enable RealUrl
    tx_realurl_enable                   = 1
    simulateStaticDocuments             = 0

    // Disable Image Upscaling
    noScaleUp                           = 1

    // Language Settings
    sys_language_uid                    = 0
    sys_language_overlay                = 1
    sys_language_mode                   = content_fallback
    language                            = de
    locale_all                          = de_DE.UTF-8
    htmlTag_setParams                   = lang="de" dir="ltr" class="no-js"

    compressJs                          = 1
    compressCss                         = 1
    concatenateJs                       = 1
    concatenateCss                      = 1

}