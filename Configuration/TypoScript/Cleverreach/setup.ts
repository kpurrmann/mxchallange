plugin.tx_formhandlercleverreach.apiKey = e67f788f22fe5b78d6c6a3df9f35f920-2
plugin.tx_formhandlercleverreach.listId = 513753
plugin.tx_formhandlercleverreach.formId = 134028
plugin.tx_formhandlercleverreach.wsdlUrl = http://api.cleverreach.com/soap/interface_v5.1.php?wsdl
plugin.mxchallenge.admin_email = kevin@purrmann-websolutions.de
plugin.mxchallenge.sender_email = entwicklung@purrmann-websolutions.de

/*
 *    Project:	mx-challenge - mx-challenge
 *    Version:	1.0.0
 *    Date:		05.08.2014 18:46:39
 *    Author:	Kevin Purrmann <entwicklung@purrmann-websolutions.de> 
 *
 *    Coded with Netbeans!
 */


plugin.Tx_Formhandler.settings {
    additionalIncludePaths {
        1 = typo3conf/ext/formhandler_cleverreach/Classes/Formhandler/
    }
}
plugin.Tx_Formhandler.settings.predef.cleverreach_example_doubleoptin {
    templateFile = EXT:mxchallenge/Resources/Private/Templates/Formhandler/Cleverreach.html
    langFile.1 = EXT:mxchallenge/Resources/Private/Language/formhandler.xml
    cssFile.1 >
    debug = 0
    errorListTemplate >
    singleErrorTemplate {
        totalWrap = <div class="alert alert-danger" role="alert">|</div>
        singleWrap = <small class="message">|</small>
    }
    masterTemplateFile = TEXT
    masterTemplateFile.value = typo3conf/ext/mxchallenge/Resources/Private/Layouts/Formhandler/mastertemplate.html

    saveInterceptors {
        10.class = Interceptor_AntiSpamFormTime
        10.config {
            # ID of a page to redirect SPAM bots to
            redirectPage = 1
            minTime.value = 20
            minTime.unit = seconds
        }
    }

    validators >
    validators {
        1 {
            class = Tx_Formhandler_Validator_Default    
            config {
                fieldConf {
                    email {
                        errorCheck.1 = required
                        errorCheck.2 = email
                        errorCheck.3 = cleverreachemailoptin
                        errorCheck.3.config < plugin.tx_formhandlercleverreach.config
                    }
                }
            }
        }
    }
    finishers {
        2 >
#        2.class = Tx_Formhandler_Finisher_Subscribe
#        2.config < plugin.tx_formhandlercleverreach.config
#        2.config {
#        
#            fields {
#                email {
#                    mapping = email
#                }
#                registered {
#                    special = sub_tstamp
#                }
#            }
#            
#        }
        1.class = Finisher_Mail
        1.config {
            mailer {
                class = Mailer_TYPO3Mailer
            }
            admin {
                sender_email = news@manufacturing-excellence.de
                sender_name = Newsletter Manufacturing Excellence Website
                to_email = news@manufacturing-excellence.de
                subject = Anmeldung für Newsletter
                attachment >
                htmlEmailAsAttachment = 0
            }
            user {
                sender_email = news@manufacturing-excellence.de
                sender_name = Newsletter Manufacturing Excellence
                to_email = email
                subject = Vielen Dank für Ihre Anmeldung zum Newsletter bei Manufacturing Excellence
                attachment >
                htmlEmailAsAttachment = 0
            }
        }
        3.class = Tx_Formhandler_Finisher_SubmittedOK
        3.config {
            returns = 1
        }
    }
}