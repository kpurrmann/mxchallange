/*
 *    Project:	mx-challenge - mx-challenge
 *    Version:	1.0.0
 *    Date:		24.07.2014 18:29:21
 *    Author:	Kevin Purrmann <entwicklung@purrmann-websolutions.de> 
 *
 *    Coded with Netbeans!
 */


tt_content.menu.20 {
    column = HMENU 
    column {
        special = list
        special.value.field = pages
        wrap = <div class="row">|</div>
        1 = TMENU
        1 {
            NO {
                doNotLinkIt = 1
                stdWrap.cObject = FLUIDTEMPLATE
                stdWrap.cObject.file = EXT:mxchallenge/Resources/Private/Partials/Teaser/3-Columns.html
                stdWrap.cObject.partialRootPath = EXT:mxchallenge/Resources/Private/Partials/Teaser/
            }
        }
    }
    full_column < .column
    full_column.wrap = |
    full_column.1.NO.stdWrap.cObject.file = EXT:mxchallenge/Resources/Private/Partials/Teaser/1-Columns.html
}