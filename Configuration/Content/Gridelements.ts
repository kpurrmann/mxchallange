/*
 *    Project:	mx-challenge - mx-challenge
 *    Version:	1.0.0
 *    Date:		24.07.2014 18:28:55
 *    Author:	Kevin Purrmann <entwicklung@purrmann-websolutions.de> 
 *
 *    Coded with Netbeans!
 */

tt_content.gridelements_pi1.20.10.setup {
	# 33/33/33
	1 < lib.gridelements.defaultGridSetup
	1 {
        stdWrap.wrap = <div class="row">|</div>
        columns {
			# colPos ID
            300 < .default
            300 {
                wrap = <div class="col-sm-4">|</div>
            }
            310 < .300
            320 < .300
        }
    }
	# 66/33
	2 < lib.gridelements.defaultGridSetup
	2 {
        stdWrap.wrap = <div class="row">|</div>
        columns {
			# colPos ID
            300 < .default
            300 {
                wrap = <div class="col-sm-8 main-content">|</div>
            }
            310 < .default
            310 {
                wrap = <aside class="col-lg-4 col-md-4 col-sm-12 col-xs-12 well">|</aside>
                required = 1
                renderObj.20.shortcut.20 {
                    conf.tt_content < tt_content
                    conf.tt_content {
                        stdWrap.outerWrap = <div class="box">|</div>
                    }
                }
            }
        }
    }
	# 100
	3 < lib.gridelements.defaultGridSetup
	3 {
        stdWrap.wrap = <div class="row main-content"><div class="col-sm-12">|</div></div>
        columns {
			# colPos ID
            300 < .default
        }
    }

    4 < .2
}

[globalVar = TSFE:id=69, TSFE:id=99]
    tt_content.gridelements_pi1.20.10.setup.2.columns.310 {
        innerWrap = TEXT
        innerWrap {
            wrap = <div class="box"><h4></h4><br><p><a href="|#footer-content" class="btn btn-default btn-block" style="background: #008ab4;color: #fff;text-shadow: none">ZUR ANMELDUNG</a></p><br></div>
            typolink = 1
            typolink {
                ATagParams =
                parameter.data=TSFE:id
                returnLast =  url
            }
        }
    }


[global]

[globalVar = TSFE:id=8]
    tt_content.gridelements_pi1.20.10.setup.2.columns.310 {
        innerWrap = TEXT
        innerWrap {
            wrap = <div class="box box-footer-hidden"><h4></h4><br><p><a href="|#c1048" class="btn btn-default btn-block" style="background: #008ab4;color: #fff;text-shadow: none">ZUR BEWERBUNG</a></p><br></div>
            typolink = 1
            typolink {
                ATagParams =
                parameter.data=TSFE:id
                returnLast =  url
            }
        }
    }


[global]