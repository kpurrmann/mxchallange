/*
 *    Project:	mx-challenge - mx-challenge
 *    Version:	1.0.0
 *    Date:		24.07.2014 18:32:51
 *    Author:	Kevin Purrmann <entwicklung@purrmann-websolutions.de> 
 *
 *    Coded with Netbeans!
 */

tt_content.bootstrap_package_carousel = COA
tt_content.bootstrap_package_carousel {
    10 >
    20 = FLUIDTEMPLATE
    20 {
        file = EXT:mxchallenge/Resources/Private/Templates/ContentElements/Bootstrap/Carousel.html
        partialRootPath = EXT:mxchallenge/Resources/Private/Partials/ContentElements/
        layoutRootPath = EXT:mxchallenge/Resources/Private/Layouts/ContentElements/
    }
}