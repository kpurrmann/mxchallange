/*
 *    Project:	mx-challenge - mx-challenge
 *    Version:	1.0.0
 *    Date:		24.07.2014 18:28:27
 *    Author:	Kevin Purrmann <entwicklung@purrmann-websolutions.de> 
 *
 *    Coded with Netbeans!
 */

lib.wellContent < lib.dynamicContent
lib.wellContent {
    20.stdWrap.wrap = <div class="well" id="footer-content"><div class="container">|</div></div>
    20.stdWrap.required = 1
}

tt_content.stdWrap.innerWrap.cObject.default.30.cObject.default.default.override >

lib.newsletter = COA_INT
lib.newsletter {
    10 = RECORDS
    10 {
        tables = tt_content
        source = 335
        dontCheckPid = 1
    }
    20 < .10
    20.source = 316
}


tt_content.table.20.stdWrap.wrap = <div class="panel panel-default"><div class="panel-body">|</div></div>

tt_content.image.20 {
    addClassesCol.override.cObject.10 {
        2 < .default
        2.value = col-sm-6 col-md-6 col-xs-12

        3 < .default
        3.value = col-md-4 col-sm-4 col-xs-12

        4 < .default
        4.value = col-md-3 col-sm-6 col-xs-12

        6 < .default
        6.value = col-md-2 col-sm-2 col-xs-12
    }
    rendering {
        singleCaption {
            rowStdWrap.wrap = <div class="image-row"> | </div>
            noRowsStdWrap.wrap = <div class="image-row"> | </div>
            lastRowStdWrap.wrap = <div class="image-row"> | </div>
            columnStdWrap.wrap = <div class="image-column###CLASSES###"> | </div>
        }
    }
}

<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mxchallenge/Configuration/Content/Bootstrap/Carousel.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mxchallenge/Configuration/Content/Gridelements.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mxchallenge/Configuration/Content/Teaser.ts">
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:mxchallenge/Configuration/Content/Navigation.ts">