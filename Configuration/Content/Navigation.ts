/*
 *    Project:	mx-challenge - mx-challenge
 *    Version:	1.0.0
 *    Date:		24.07.2014 18:29:12
 *    Author:	Kevin Purrmann <entwicklung@purrmann-websolutions.de> 
 *
 *    Coded with Netbeans!
 */

lib.navigation.main >
lib.navigation.main = HMENU
lib.navigation.main {
    special = directory
    special.value = 2
    wrap = <ul id="button-navigation" class="nav navbar-nav">|</ul>
    1 = TMENU
    1 {
        expAll = 1
        noBlur = 1
        NO {
            wrapItemAndSub = <li>|</li>
        }

        ACT = 1
        ACT {
            wrapItemAndSub = <li class="active">|</li>
        }

        CUR = 1
        CUR < .ACT
    }
    2 < .1
    2 {
        wrap = <nav class="sub-navigation"><span class="triangle triangle-navigation hidden-sm hidden-md hidden-xs">&nbsp;</span><ul class="nav navbar-nav">|</ul></nav>
        NO {
            wrapItemAndSub = <li>|</li>
        }
    }
}

lib.navigation.meta = HMENU
lib.navigation.meta {
    special = directory
    special.value = 3
    wrap = <ul id="main-navigation" class="nav navbar-nav navbar-right" >|</ul>
    1 = TMENU
    1 {
        expAll = 1
        noBlur = 1
        NO {
            wrapItemAndSub = <li>|</li>
        }
        ACT = 1
        ACT {
            wrapItemAndSub = <li class="active">|</li>
        }

        CUR = 1
        CUR < .ACT
    }
    2 < .1
    2 {
        wrap = <nav class="sub-navigation"><span class="triangle triangle-navigation hidden-sm hidden-md hidden-xs">&nbsp;</span><ul class="nav navbar-nav">|</ul></nav>
        NO {
            wrapItemAndSub = <li>|</li>
        }
    }
}

lib.navigation.service = HMENU
lib.navigation.service {
    special = directory
    special.value = 4
    wrap = <ul id="meta-navigation" class="nav navbar-nav navbar-right" >|</ul>
    1 = TMENU
    1 {
        expAll = 1
        noBlur = 1
        NO {
            wrapItemAndSub = <li>|</li>
        }
        ACT = 1
        ACT {
            wrapItemAndSub = <li class="active">|</li>
        }

        CUR = 1
        CUR < .ACT
    }
    2 < .1
    2 {
        wrap = <nav class="sub-navigation"><span class="triangle triangle-navigation hidden-sm hidden-md hidden-xs">&nbsp;</span><ul class="nav navbar-nav">|</ul></nav>
        NO {
            wrapItemAndSub = <li>|</li>
        }
    }
}

lib.navigation.breadcrumb {
    wrap = <div class="breadcrumb-section"><div class="container">|</div></div>
    10.includeNotInMenu = 0
    10.special.range = 0|-1
}


lib.logo = IMAGE
lib.logo {
    file = EXT:mxchallenge/Resources/Public/img/logo-xs.png
    params = class="logo"
    altText = Manufacturing Excellence Logo
    imageLinkWrap = 1
    imageLinkWrap.enable = 1
    imageLinkWrap.typolink = 1
    imageLinkWrap.typolink.parameter = 1
}